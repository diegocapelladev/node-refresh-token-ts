# Node Refresh Token

- Criar o `package.json` com `yarn init -y`
- Add as dependências de desenvolvimento:
`yarn add --dev typescript @types/express ts-node-dev`

- Add o express:
`yarn add express`

- Inicia o typescript:
`yarn tsc --init`

- Criar a pasta **src** e dentro de src criar o arquivo `server.ts`:

```
import express from 'express'

const app = express()

app.listen(3333, () => console.log("Server is running on port 3333!"))
```

- Criar o **scripts** em `package.json`

```
"scripts": {
    "dev": "ts-node-dev src/server.ts"
  },
```

- Instala o prisma como dependências de desenvolvimento: `yarn add --dev prisma` e o client `yarn add @prisma/client`

- Inicia o prisma com: `yarn prisma init`

- Será criado a pasta **prisma** e o arquivo `.env` para configurar o banco de dados:

- No arquivo `schema.prisma` cria o model da tabela do DB:

```
model User {
  id        String @id @default(uuid())
  name      String
  username  String @unique
  password  String
}
```

- Criar a migrations; `yarn prisma migrate dev`, depois passa um nome para a migration <create_users>

- Cria a pasta **prisma** com o arquivo `client.ts` dentro de src, para ser a classe de conexão:

```
import { PrismaClient } from '@prisma/client'

const client = new PrismaClient()

export { client }
```

- Add o bcryptjs: `yarn add bcryptjs` e depois as tipagens `yarn add --dev @types/bcryptjs`

- Cria o arquivo `src/useCases/authenticateUser/CreateUserUseCase.ts`:

```
import { client } from "../../prisma/client"

import { hash } from "bcryptjs"

interface IUSerRequest {
  name: string
  password: string
  username: string
}

class CreateUserUseCase {
  async execute({ name, username, password }: IUSerRequest) {
    // Verificar se o usuário existe
    const userAlreadyExists = await client.user.findFirst({
      where: {
        username
      }
    })

    if (userAlreadyExists) {
      throw new Error("User already exists!")
    }
    const passwordHash = await hash(password, 8)

    const user = await client.user.create({
      data: {
        name,
        username,
        password: passwordHash
      }
    })

    return user
  }
}

export { CreateUserUseCase }
```
- Cira o controller: `src/useCases/authenticateUser/CreateUserController.ts`

```
import { Request, Response } from "express";
import { CreateUserUseCase } from "./CreateUserUseCase";

class CreateUserController {
  async handle(request: Request, response: Response) {
    const { name, username, password } = request.body

    const createUserUseCase = new CreateUserUseCase()

    const user = await createUserUseCase.execute({
      name,
      username,
      password
    })

    return response.json(user)
  }
}

export { CreateUserController }
```

- Cria o arquivo de rota: `src/routes.ts`

```
import { Router } from "express"
import { CreateUserController } from "./useCases/authenticateUser/CreateUserController"

const router = Router()

const createUserController = new CreateUserController()

router.post("/users", createUserController.handle)

export { router }
```
- Altera o `server.ts`

```
import express from 'express'
import { router } from './routes'

const app = express()

app.use(express.json())

app.use(router)

app.listen(3333, () => console.log("Server is running on port 3333!"))
```

- Add o `yarn add express-async-errors` para tratamento de erros, pois o express não lida muito bem com tratamento de erros

- Altera o `service.ts`

```
import 'express-async-errors'
import express, { NextFunction, Request, Response } from 'express'
import { router } from './routes'

const app = express()

app.use(express.json())

app.use(router)

app.use((error: Error, request: Request, response: Response, next: NextFunction) => {
  return response.json({
    status: "Error",
    message: error.message
  })
})

app.listen(3333, () => console.log("Server is running on port 3333!"))
```

- Add o JWT: `yarn add jsonwebtoken` e as tipagens `yarn add --dev @types/jsonwebtoken`

- Cria o arquivo `src/useCases/authenticateUser/AuthenticateUserUseCase.ts`

```
import { compare } from "bcryptjs"
import { sign } from "jsonwebtoken"
import { client } from "../../prisma/client"


interface IRequest {
  username: string
  password: string
}

class AuthenticateUserUseCase {
  async execute({ username, password }: IRequest) {
    const userAlreadyExists = await client.user.findFirst({
      where: {
        username
      }
    })

    if (!userAlreadyExists) {
      throw new Error("User or password incorrect!")
    }

    const passwordMatch = await compare(password, userAlreadyExists.password)

    if (!passwordMatch) {
      throw new Error("User or password incorrect!")
    }

    const token = sign({}, "f7a3fe8d-439d-406c-956a-38cb81e672ae", {
      subject: userAlreadyExists.id,
      expiresIn: "20s"
    })

  return { token }
  }
}

export { AuthenticateUserUseCase }
```

- Cria o arquivo `src/useCases/authenticateUser/AuthenticateUserController.ts`

```
import { Request, Response } from "express";
import { AuthenticateUserUseCase } from "./AuthenticateUserUseCase";


class AuthenticateUserController {
  async handle(request: Request, response: Response) {
    const { username, password} = request.body

    const authenticateUserUseCase = new AuthenticateUserUseCase()

    const token = await authenticateUserUseCase.execute({
      username,
      password
    })

    return response.json(token)
  }
}

export { AuthenticateUserController }
```

- Alterar `routes.ts`

```
import { request, response, Router } from "express"
import { AuthenticateUserController } from "./useCases/authenticateUser/AuthenticateUserController"
import { CreateUserController } from "./useCases/createUser/CreateUserController"

const router = Router()

const createUserController = new CreateUserController()
const authenticateUserController = new AuthenticateUserController()

router.post("/users", createUserController.handle)
router.post("/login", authenticateUserController.handle)

router.get("/courses", (request, response) => {
  return response.json([
    { id: 1, name: "ReactJS" },
    { id: 2, name: "NodeJS" },
    { id: 3, name: "React Native" },
  ])
})

export { router }
```

- Cria o middleware para verificar as rotas autenticadas: `src/middleware/ensureAuthenticated.ts`

```
import { NextFunction, Request, Response } from "express";
import { verify } from "jsonwebtoken";


export function ensureAuthenticated(request: Request, response: Response, next: NextFunction) {
  const authToken = request.headers.authorization

  if (!authToken) {
    return response.status(401).json({
      message: "Unauthorized"
    })
  }

  const [, token] = authToken.split(" ")

  try {
    verify(token, "f7a3fe8d-439d-406c-956a-38cb81e672ae")

    return next()
  } catch(err) {
    return response.status(401).json({
      message: "Token Invalid"
    })
  }
}
```

- Add o middleware na rota: `router.get("/courses",  ensureAuthenticated, (request, response) => { ... })`

- Alterar `prisma/schema.prisma`:

```
model User {
  id        String @id @default(uuid())
  name      String
  username  String @unique
  password  String
  refresh_token RefreshToken?

  @@map("users")
}

model RefreshToken {
  id        String  @id @default(uuid())
  expiresIn Int
  user      User    @relation(fields: [userId], references: [id])
  userId    String  @unique

  @@map("refresh_token")
}
```
- Add `yarn add dayjs`

- Criar refresh token `src/provider/GenerateRefreshToken.ts`

```
import dayjs from 'dayjs'
import { client } from "../prisma/client";

class GenerateRefreshToken {

  async execute(userId: string) {
    const expiresIn = dayjs().add(15, "second").unix()

    const generateRefreshToken = await client.refreshToken.create({
      data: {
        userId,
        expiresIn
      }
    })

    return generateRefreshToken
  }
}

export { GenerateRefreshToken }
```