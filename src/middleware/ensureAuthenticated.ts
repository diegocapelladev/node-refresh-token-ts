import { NextFunction, Request, Response } from "express";
import { verify } from "jsonwebtoken";


export function ensureAuthenticated(request: Request, response: Response, next: NextFunction) {
  const authToken = request.headers.authorization

  if (!authToken) {
    return response.status(401).json({
      message: "Unauthorized"
    })
  }

  const [, token] = authToken.split(" ")

  try {
    verify(token, "f7a3fe8d-439d-406c-956a-38cb81e672ae")

    return next()
  } catch(err) {
    return response.status(401).json({
      message: "Token Invalid"
    })
  }
}