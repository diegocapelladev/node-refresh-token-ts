import { sign } from "jsonwebtoken"


class GenerateTokenProvider {
  async execute(userId: string) {
    const token = sign({}, "f7a3fe8d-439d-406c-956a-38cb81e672ae", {
      subject: userId,
      expiresIn: "20s"
    })

    return token
  }
}

export { GenerateTokenProvider }